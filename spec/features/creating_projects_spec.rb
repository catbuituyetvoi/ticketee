require 'spec_helper'

feature 'Creating Projects' do
	scenario 'can creaate a project' do
		visit '/'
		click_link 'New Project'
		fill_in "Name",	with: "TextMate 2"
		fill_in "Description",	with: "A text-editor"
		click_button 'Create Project'
		expect(page).to have_content("Project ok")
	end	  
end