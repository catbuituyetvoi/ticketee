class ProjectsController < ApplicationController
	#new Projects
	def new
		@project = Project.new
	end

	def create
		@project = Project.new(project_params)
		if @project.save
			flash[:notice] = "project has been creaated"
			redirect_to @project
		else
			#nothing , yet
		end
	end

	def show
		@project = Project.find(params[:id])
	end

	def index
		@projects = Project.first(3)
	end

	private
	def project_params
		params.require(:project).permit(:name, :description)
	end
end
